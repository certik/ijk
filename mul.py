from numpy import array, dot

def M(a, b, c, d):
    """
    Returns a matrix that represents a quaternion a+bi+cj+dk.
    """
    return array([[a+b*1j, c+d*1j], [-c+d*1j, a-b*1j]])

def q(A):
    """
    Return the (a, b, c, d) of the quaternion a+bi+cj+dk represented by the
    matrix "A".
    """
    return (A[0,0].real, A[0,0].imag, A[0,1].real, A[0,1].imag)

U = M(1, 0, 0, 0)
I = M(0, 1, 0, 0)
J = M(0, 0, 1, 0)
K = M(0, 0, 0, 1)
d = {"i": I, "j": J, "k": K}

f = open("ijk.txt")
r = U
for letter in f.read():
    r = dot(r, d[letter])

print "The final matrix is:"
print r
print "The corresponding quaternion (a, b, c, d) = a+bi+cj+dk is:"
print q(r)
print "The result should be -j:"
print -J
print q(-J)
