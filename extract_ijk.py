print "Reading finnegans-wake.txt"
f = open("finnegans-wake.txt")
ijk = ""
for letter in f.read():
    if letter in ["i", "j", "k"]:
        ijk += letter
print "Found %d i's, j's, k's letters" % len(ijk)
print "Saving to ijk.txt"
f = open("ijk.txt", "w")
f.write(ijk)
