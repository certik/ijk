# Wolfram Alpha, Finnegans Wake, and Quaternions

This code answers the question:

> result of extracting the i's, j's, and k's in order from Finnegans Wake and interpreting as a quaternion product

posed by https://www.johndcook.com/blog/2017/08/02/wolfram-alpha-finnegans-wake-and-quaternions/.

Here is how to run it:
```
$ python extract_ijk.py
Reading finnegans-wake.txt
Found 77795 i's, j's, k's letters
Saving to ijk.txt
$ python mul.py
The final matrix is:
[[ 0.+0.j -1.+0.j]
 [ 1.+0.j  0.+0.j]]
The corresponding quaternion (a, b, c, d) = a+bi+cj+dk is:
(0.0, 0.0, -1.0, 0.0)
The result should be -j:
[[-0.-0.j -1.-0.j]
 [ 1.-0.j -0.-0.j]]
(-0.0, -0.0, -1.0, -0.0)
```
The answer is `-j`.
